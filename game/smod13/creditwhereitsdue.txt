Rara: Project lead/lead coder.
Msalinas: Major coding help (mainly with MapAdd system).
Sparrowstar: General coding help.
Arthurdead: General coding help.
The Renegadist (aka Chr0n0Tr!gg3r): Getting the word out.
Joshy: Coding consultation.

SirMaster: Kick, base of mapadd and custom weapon system.
Bitl: Various bits from his mods, FireFight: Reloaded and SURVIVOR.
Soldier11, futon, Strelok, tigg: M16 weapon assets.
EvilGarlic, SpeisCheese, Velly: Combine Synth Soldier assets.
JimTheCookie: Extra melee animations for Combine Soldiers (and related enemies).
[L]OST M.D.: HL1 Gordon playermodels.
Captain Charles, Nightstalker: Viewmodel arms for HL1 punch and glove admire animation, chrome fix for all HL1 models.
Sergeant Stacker: Coding and assets for Combine Guard and Cremator enemies.
Maestro Fénix: Coding and assets for Combine Advisor enemy.
Comfort Jones: Most of the enhanced Combine Soldier AI.

Original SMOD Team: Idea, conscept, and the source of a lot of the assets in this.
Obsidian Conflict Team: Various assets, including but not limited too: Alyxgun weapon assets, manhack weapon assets, HD Human Grunt assets.
Half-Life 2 Sandbox Team: Custom entity base scripting, code for physgun weapon.
Missing Information Team: Various assets, including but not limited too: hopwire weapon assets, physgun weapon assets, combine guard cannon weapon assets.
Synergy Team: Various assets, including but not limited too: Deagle weapon assets, various vehicle assets, mattspipe viewmodel
Transmissions Element 120 team: Various graphical enhancements, gameplay source code for their mod.
Nightmare House 2 team: Gameplay source code for their mod.

VALVe: Without them, none of this would be possible.
Gearbox: Minor things ported from Opposing Force.
Running With Scissors: Various small sound effects, possible inspiration for some original SMOD weapons and mechanics (such as the shovel, scissors and kick)
