// Alyx Gun

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"				"#SMOD_AlyxGun"
	"viewmodel"				"models/weapons/V_Alyx_Gun.mdl"
	"playermodel"			"models/weapons/W_Alyx_Gun.mdl"
	"anim_prefix"			"alyxgun"
	"bucket"				"pistol"
	"bucket_position"		"2"

	"clip_size"				"30"
	"clip2_size"			"-1"

	"default_clip"			"30"
	"default_clip2"			"-1"

	"primary_ammo"			"AlyxGun"

	"item_flags"			"0"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"reload"		"Weapon_AlyxGun.Reload"
		"reload_npc"	"Weapon_Alyx_Gun.NPC_Reload"
		"empty"			"Weapon_AlyxGun.Empty"
		"single_shot"		"Weapon_Alyx_Gun.NPC_Single"
		"single_shot_npc"	"Weapon_Alyx_Gun.NPC_Single"
		"special1"		"Weapon_AlyxGun.Special1"
		"special2"		"Weapon_AlyxGun.Special2"
		"burst"			"Weapon_Alyx_Gun.NPC_Single"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"ObsidianWeaponIcons"
				"character"	"E"
		}
		"weapon_s"
		{	
				"font"		"ObsidianWeaponIconsSelected"
				"character"	"E"
		}
		"weapon_small"
		{
				"font"		"ObsidianWeaponIconsSmall"
				"character"	"E"
		}
		"ammo"
		{
				"font"		"ObsidianWeaponIconsSmall"
				"character"	"e"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}