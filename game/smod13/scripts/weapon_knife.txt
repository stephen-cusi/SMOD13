// Crowbar

WeaponData
{
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"	"#SMOD_Knife"
	"viewmodel"				"models/weapons/v_knife_t.mdl"
	"playermodel"			"models/weapons/w_knife_t.mdl"
	"anim_prefix"			"crowbar"
	"bucket"				"melee"
	"bucket_position"		"3"

	"clip_size"				"-1"
	"primary_ammo"			"None"
	"secondary_ammo"		"None"

	"weight"				"0"
	"item_flags"			"0"
	"addfov"				"20"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"single_shot"		"Weapon_Crowbar.Single"
		"melee_hit"			"Weapon_Crowbar.Melee_Hit"
		"melee_hit_world"	"Weapon_Crowbar.Melee_HitWorld"
		"deploy"			"Weapon_Knife.Deploy"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"CSWeaponIcons"
				"character"	"j"
		}
		"weapon_s"
		{	
				"font"		"CSWeaponIconsSelected"
				"character"	"j"
		}
		"weapon_small"
		{	
				"font"		"CSWeaponIconsSelected"
				"character"	"j"
		}
		"ammo"
		{
				"font"		"CSWeaponIcons"
				"character"	"j"
		}
		"crosshair"
		{
				"font"		"Crosshairs"
				"character"	"Q"
		}
		"autoaim"
		{
			"file"		"sprites/crosshairs"
			"x"			"0"
			"y"			"48"
			"width"		"24"
			"height"	"24"
		}
	}
}